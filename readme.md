readme.md - README for GambasAppUtilityLib v0.4


Purpose:
To encapsulate common functionality, reduce custom coding needed to start a new project, and provide consistency across projects. 
Library for common application functions such as error-message  or help-about formatting. 

Note: Gambas3 projects ('.project') are looking for libraries ('<filename>.gambas') in /home/<user_name>/.local/share/gambas3/lib/<vendor_prefix>/.Make (Project|Make|Executable...) writes the executable there in addition to the root of the project's directory.


Usage notes:

~...


Setup:
0.4: 
~add license file
~add readme
0.3:
~internationalization prep; edit strack trace output. 
0.2: 
~re-release under new name. 
0.1: 
~initial release with Log and Dialogs classes under old name 'AppUtility'.

Fixes:

Known Issues:
~

Possible Enhancements:


Steve Sepan
ssepanus@yahoo.com
2/14/2022